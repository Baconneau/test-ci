#!/bin/sh

# import the key
gpg --recv-key $1

if gpg --fingerprint $1 2>&1 | grep -f fingerprint.txt # test that the key we downloaded if the official one.
then
    echo Successfully verified fingerprint
else
    echo Problem with fingerprint.
    exit 1
fi

if gpg --verify $2 # test the binary's integrity.
then
    echo Successfully verified signature.
else
    echo Problem with signature.
    exit 1
fi
