# test-ci

This is a test project that:

- Build a Docker image
- Scan it
- Publish it 
- Create a Helm chart 
- Deploy the chart
